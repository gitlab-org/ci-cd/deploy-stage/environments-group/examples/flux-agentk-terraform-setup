# Flux+agentk Terraform setup

This is an example repository to set up Flux and the GitLab agent for Kuberentes with Terraform. See [the documentation for related instructions](https://docs.gitlab.com/ee/user/clusters/agent/gitops/flux_terraform_tutorial.html).
