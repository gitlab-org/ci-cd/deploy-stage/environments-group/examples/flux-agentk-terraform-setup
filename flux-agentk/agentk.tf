resource "gitlab_cluster_agent" "this" {
  project = data.gitlab_project.this.id
  name    = var.agent_name_gitlab
}

resource "gitlab_cluster_agent_token" "this" {
  project     = data.gitlab_project.this.id
  agent_id    = gitlab_cluster_agent.this.agent_id
  name        = "my-agent-token"
  description = "Token for the agent used with `gitlab-agent` Helm Chart"
}

resource "kubernetes_namespace" "agent" {
  metadata {
    name = var.agent_namespace
  }

  lifecycle {
    ignore_changes = [
      metadata[0].labels,
    ]
  }
}

resource "kubernetes_secret" "agent_token" {
  depends_on = [
    kubernetes_namespace.agent
  ]

  metadata {
    name      = "${var.agent_name_cluster}-token"
    namespace = var.agent_namespace
  }

  data = {
    token       = gitlab_cluster_agent_token.this.token
  }
}

resource "gitlab_repository_file" "agentk" {
  project        = data.gitlab_project.this.id
  branch         = data.gitlab_project.this.default_branch
  file_path      = "${var.target_path}/agentk.yaml"
  content        = base64encode(templatefile("${path.module}/agentk-helmrelease.tftpl", { 
    agent_name_cluster=var.agent_name_cluster, 
    agent_namespace=var.agent_namespace,
    }))
  commit_message = "Add ${var.target_path}/agentk.yaml"

  depends_on = [flux_bootstrap_git.this]
}
