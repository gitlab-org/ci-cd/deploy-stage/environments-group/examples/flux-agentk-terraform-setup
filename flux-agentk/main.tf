terraform {
  required_providers {
    gitlab = {
      source = "gitlabhq/gitlab"
      version = "15.11.0"
    }
    flux = {
      source  = "fluxcd/flux"
      version = "1.0.1"
    }
  }
  backend "http" {
  }
}

provider "gitlab" {
  token = var.gitlab_token
}
