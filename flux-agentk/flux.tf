# Flux
provider "flux" {
  kubernetes = {
    config_path = "${var.kubeconfig}"
  }
  git = {
    url = "https://gitlab.com/${var.project_slug}.git"
    http = {
      username = "GitLab token"
      password = "${var.gitlab_token}"
    }
  }
}

resource "gitlab_deploy_token" "flux" {
  project    = data.gitlab_project.this.id
  name       = "Flux default repository access"
  username   = "flux-repository-token2"

  scopes = ["read_repository"]
}

resource "kubernetes_namespace" "flux" {
  metadata {
    name = "flux-system"
  }

  lifecycle {
    ignore_changes = [
      metadata[0].labels,
    ]
  }
}

resource "kubernetes_secret" "flux_secret" {
  depends_on = [
    gitlab_deploy_token.flux
  ]

  metadata {
    name      = "flux-system"
    namespace = "${kubernetes_namespace.flux.metadata[0].name}"
  }

  data = {
    username       = "${gitlab_deploy_token.flux.username}"
    password       = "${gitlab_deploy_token.flux.token}"
  }
}

resource "flux_bootstrap_git" "this" {
  depends_on = [gitlab_deploy_token.flux]

  path = "${var.target_path}"
  disable_secret_creation = true
  secret_name = "${kubernetes_secret.flux_secret.metadata[0].name}"
}
