variable "gitlab_token" {
  type = string
  sensitive = true
}

variable "project_slug" {
  type = string
  description = "The current project's slug"
}

variable "branch" {
  description = "branch name"
  type        = string
  default     = "main"
}

variable "target_path" {
  description = "flux sync target path"
  type        = string
  default     = "manifests/flux-system"
}

variable "agent_name_gitlab" {
  default = "nagyv-linode"
  description = "Agent name on GitLab, should be a valid directory name to host the configuration file"
}

variable "agent_name_cluster" {
  default = "gitlab-agent"
  description = "Agent name on the cluster"
}

variable "agent_namespace" {
  default     = "gitlab-agent"
  description = "Kubernetes namespace to install the Agent"
}

variable "kubeconfig" {
  description = "Set in CI from KUBE_CONFIG_PATH"
}
