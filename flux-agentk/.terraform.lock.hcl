# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/fluxcd/flux" {
  version     = "1.0.1"
  constraints = "1.0.1"
  hashes = [
    "h1:9Dr5VtZfLSPekZIie3ukbM8aA8lbaOu2qLH+J4/9Ih4=",
    "h1:G15ev70jy2IldjRb+h1i9wQsbYzNqtYI6PSenoJADz4=",
    "h1:GstHRAoGxZX3yPE78fBUIdhoNzMIe54OJvcibllfjSE=",
    "h1:I8+oqI3/eoD5R8L3169AHK7Qr9I+nRzC+EiU3Y//jbA=",
    "h1:MP+ca6EeazhGv3npasLupyB3Tlj7zDghA1UgimRiJ1w=",
    "h1:NhUwfzNDCmDIcb0V6EskNAoMyd26sRAPsN/dOBViGpg=",
    "h1:SWI88/1XPZ8zOdqVJ7BPjZX+kxFcrVIUSmIVAnruy64=",
    "h1:Ys9q28HZP1wI41GMCTZTxDrDSePuurfPkNwCiubBrFE=",
    "h1:aQFTp+tBKWyF4pRzJU6FbjS1RmXcviPDZDg7XKdh/oQ=",
    "h1:e3sd5jVFYMkorp5DhSXTUQJ5qr1aO6clKr89FY/32tE=",
    "h1:oNL9XMW47/YKG3s84XViGG8Ml8rkJhrah214UVS24r8=",
    "h1:q6rTeG5wyo7c/fFjY+lZpgxVBnlv/VUqCRpBNDxCTR0=",
    "h1:vMnuOAej8zRCLYM6u8HVPzXzbLC8gHRD6EllwCao6T4=",
    "h1:xaYF5hj3y+x3ruGuuMlJvqPcWhH18iVO4S9/ZkGyCOo=",
  ]
}

provider "registry.terraform.io/gavinbunney/kubectl" {
  version     = "1.14.0"
  constraints = ">= 1.7.0"
  hashes = [
    "h1:mX2AOFIMIxJmW5kM8DT51gloIOKCr9iT6W8yodnUyfs=",
    "zh:0350f3122ff711984bbc36f6093c1fe19043173fad5a904bce27f86afe3cc858",
    "zh:07ca36c7aa7533e8325b38232c77c04d6ef1081cb0bac9d56e8ccd51f12f2030",
    "zh:0c351afd91d9e994a71fe64bbd1662d0024006b3493bb61d46c23ea3e42a7cf5",
    "zh:39f1a0aa1d589a7e815b62b5aa11041040903b061672c4cfc7de38622866cbc4",
    "zh:428d3a321043b78e23c91a8d641f2d08d6b97f74c195c654f04d2c455e017de5",
    "zh:4baf5b1de2dfe9968cc0f57fd4be5a741deb5b34ee0989519267697af5f3eee5",
    "zh:6131a927f9dffa014ab5ca5364ac965fe9b19830d2bbf916a5b2865b956fdfcf",
    "zh:c62e0c9fd052cbf68c5c2612af4f6408c61c7e37b615dc347918d2442dd05e93",
    "zh:f0beffd7ce78f49ead612e4b1aefb7cb6a461d040428f514f4f9cc4e5698ac65",
  ]
}

provider "registry.terraform.io/gitlabhq/gitlab" {
  version     = "15.11.0"
  constraints = "15.11.0"
  hashes = [
    "h1:3RLP8MGVIkcTL0SfIIj7myZKEkc2DlsWZked7MnNDNk=",
    "zh:2310bbf46e93ba98eb3746f4b06b6e25e8097afcd29c8d163fe8ef53459b3bd0",
    "zh:36d3bcde4caa47803e594bd8cd78833ea79423c2eb3345955b673c4b4ec85e6b",
    "zh:3a5c102b55854470dbbe7557f8fc336502125440fe7de22675e1a0e305fcdeab",
    "zh:629620caebd370892df2bfd2662c753a4bae24370cd19b1a9af3c41b6f87f4c0",
    "zh:66c2d40daefa8046dfa327c5d83dfb31921b759b521ea72c9df8f7a1cd749d0e",
    "zh:6d10fb524cfbb853d3fc9b007643611714ae7b6089a6cd495357b016ffbd57ae",
    "zh:cd69864ba6ca1a608b3d7fccdad3e4b6836502a78fe85677231eb47860f7d3a7",
    "zh:d0e8005caa6584ece7e91b736ca8b671d3b94d4b689ecd39c3670f5439ca4807",
    "zh:d5d302a7f6e98948abae85823c714686bb4cdc74d68c1f6106a307a216c228bc",
    "zh:d7ca0b13218310e7bf2ff98f86e907ec4a3a2d8aa241ad3f3067374081456706",
    "zh:db3e19cc92d475b909ba74f2c1c948c317f7c525b8e5d3078607062edca6ca32",
    "zh:dff6565297b59306661a2a5e0bb354192b6ab5812d635ffb64d4e1d15536abd2",
    "zh:f646e1015732d5ee6291f535fff13afcbfa834b024e3baf82d0a5f99d28597a8",
    "zh:f809ab383cca0a5f83072981c64208cbd7fa67e986a86ee02dd2c82333221e32",
    "zh:ff4b61b6f4723ee25cf40a3c44c1a29d93b81d2ba179d9d7d11d489aa75a335f",
  ]
}

provider "registry.terraform.io/hashicorp/kubernetes" {
  version     = "2.20.0"
  constraints = ">= 2.0.2"
  hashes = [
    "h1:5BEsAlCSlJ04X5CDpDv76JkrS/T9UsmO8C7B/b6qNFc=",
    "h1:A+uNz4B7oHqQA2lRX6u62MomG7s+uXlMpzNyBL0xIXg=",
    "h1:E7VAZorKe5oXn6h1nxP3ROwWNiQSrZlTawzix1sh8kM=",
    "h1:K6zaEIB8fW+KOQa9hI24/125t6oX8a11RAOOQQiRVDA=",
    "h1:LOMhHrJ3pLpnpCT8xpa/WAJ3HYuW3cQWiElassC+df4=",
    "h1:PGIrfrDuSPcGkQNzIbc0r45471E+t59eibVD+tUMCp4=",
    "h1:Xe79v44FUVUxxZz8GQ9puZoW+AsNGyJmTB58RG9cjkw=",
    "h1:fxmvt3S6QVBXR+T2VKRHWOzvuiY+eF4skljoXuGb33g=",
    "h1:gCFRJokcVbZ5wloAxLqUtCJ9/+cShl3jAhSNjFJxom8=",
    "h1:o4xGg9azHGpkBG8jwi71iO3KymBvAtsxrLNrTGfy2IY=",
    "h1:qMtReI9kGTwolTydGZ7qiHIasWZU6p6hSGkTMK3Emt4=",
  ]
}
